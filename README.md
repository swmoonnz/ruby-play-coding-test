# Instructions

The following instructions are for the Full Stack Developer and Test Engineer roles. You only need to do the section for the role you are applying for. If you applied for both, then complete both sections.

## Coding test for Full Stack Developer

We want to test your technical skills at both front-end and back-end development.

### Functional Requirements

1. A search form that allows users to submit the name of a movie.
2. The search will retrieve data from the Open Movie Database API (https://omdbapi.com). We have created an API key for you: 32cd7c76.
3. Present the name of the movie, year of release, director, actors and plot summary in a visually appealing manner. If the search returns no result, display a message to the user.
4. Users should be able to select a movie as a "favourite".
5. On a separate screen or tab, list movies that have been favourited. The list of favourite movies should be presented in a table.
6. Modify the menu to match the contents of the site.
7. Instructions for the user.
8. Responsive design.

### What we're looking for

- A responsive layout that handles a range of screen sizes and orientations.
- The process you used to complete this test.
- Code reusability.
- Extensability and maintainability of the software.
- Use of appropriate framework capabilities.
- Your creativity in designing the site.

### Extra credit

- Any extra features or polish you want to include.

### Technologies

We have provided a starter site using the Quasar Framework, built on Vue JS.

- [Quasar](https://quasar.dev)
- [Vue JS](https://vuejs.org)

The starter site includes Axios, Vuex and Typescript support - use of any of these technologies is optional. Any Quasar components you make use of in the project will be automatically imported.

### How do I get set up?

- [Fork](https://bitbucket.org/lootwinner/ruby-play-coding-test/fork) this repository to your Bitbucket account, then clone it to your local machine.
- Create your site in your new repository.
- Install the dependencies: type `npm install` in the command line or terminal from the repository directory.
- To run the site on a local server, type `quasar dev` in the command line or terminal from the repository directory.

### Deliverables

Code should be committed to a git repository hosted on [Bitbucket.org](https://bitbucket.org)

## Coding test for Test Engineer

We want to test your ability to write an end-to-end test on our current PWA (Progressive Web Application)

### Functional Requirements

1. You will need to write a test for the login and display name functionalities on [Wild Ruby Casino PWA](https://play.test.wildrubycasino.com), note that this is a staging version of the PWA.
2. The login test flow will include:
   - Navigate to https://play.test.wildrubycasino.com
   - Click on the `Login` button.
   - Enter the following login informaton
     - username: `test@wildrubynetwork.com`
     - password: `Ruby1234`
   - On a successfull login, the user is redirected to the PWA Lobby page.
3. Repeat the login test for the following scenarios, the test should success if the user cannot login:
   - Wrong username and correct password.
   - Correct username and wrong password.
   - Wrong username and wrong password.
4. The display name test flow will include:
   - After a successfull login, click on the user icon on the bottom right corner to navigate to the Player Details page.
   - Enter a randomly generated name in the `Display Name` field and submit it.
   - Validate that the display name has been changed successfully.

### Tools and Technologies

- You will need to make some choices to implement the end-to-end test:
  - A testing framework (e.g. Selenium, Cypress).
  - A test runner (e.g. Mocha, Jest).
  - A browser for the test (e.g. Chrome, Firefox, Headless Chrome).
- You can write the tests either in JavaScript or TypeScript.
- The test will not be evaluated on which tools you choose but rather on how well the chosen tools are used.

### What we're looking for

- A well designed and consistent test suite.
- Code reusability.
- Extensability and maintainability of the test suit.
- Use of appropriate framework capabilities.
- Your creativity in designing the test suite.

### How do I get set up?

- [Fork](https://bitbucket.org/lootwinner/ruby-play-coding-test/fork) this repository to your Bitbucket account, then clone it to your local machine. (If you are doing both the Full Stack Developer and Test Engineer tests, then you can use the same forked repository for both)
- Add the tests your new repository.
- Install the dependencies: type `npm install` in the command line or terminal from the repository directory.
- Modify the `test` script in `package.json` to run your test based on the tools you have chosen. You should be able to run the test by typing `npm run test` in the terminal.

### Deliverables

Code should be committed to a git repository hosted on [Bitbucket.org](https://bitbucket.org)
